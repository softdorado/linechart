# Changelog

## 1.3.0 (14 Aug 2023)
* The data on axis-X may be data-time or numerical values. To switch between type of chart use parameter **axisXType**.
* New parameter for line **connectIfMissing**. It define how to draw line if part of data are missing.
* All internal properties in class LineChart rename with prefix _. 
* Rewrite comments by JS standard https://jsdoc.app

## 1.2.2 (20 Nov 2022)
* In README change link to demo and move to new repository.

## 1.2.0 (01 Nov 2020)
* New feature:
	- By click on legend item hide/show line on chart.
* Fix bug:
	- In some case label on axe-y below axe-x.

## 1.1.0 (21 May 2020)
Allowed negative and positive values in randge from -9007199254740991 till 9007199254740991.

## 1.0.1 (4 May 2020)
- Add README.md
- Add directory **src** (files for development) and **dist** (ready to use minified files)

## 1.0.0 (26 April 2020)
- First Version

