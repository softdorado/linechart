// Comments https://jsdoc.app/

/**
 * @version 1.3.0
 * @author Andrej Koslov
 */

/**
 * The oject represent the chart. Constructor create object and HTML code for the chart. The chart 
 * uses external CSS styles .nowrap .linechart, .chart-title, .chart-live-data, .chart-legend,
 * .chart-legent-item, .chart-legent-color. The chart uses global variable isMobil (Boolean). If 
 * false, show live data by mouse over. The line create from LineData (see above) Labels on axis X
 * and Y will be created automaticaly. The chart can show maximal 10 lines in different colors.
 * @class 
 * @param reference to parent element for chart. If null use HTML BODY as parent element.
 * @param chartId {number} identifier for chart. Optional. If not defined use random number.
 * @param width {number} width in pixel. Optional. Default use available width in parent element.
 * @param title {string} title of chart. Optional. If null don't create title.
 * @param className {string} class for div of chart. Optional. Default "linechart"
 * @param axisXType {string} for time based values on axis X set "time" (Default). For number based 
 * values "numer". 
 */
class LineChart {
    constructor(container, chartId, width, title, className, axisXType) {
        if(container == null)
            container = document.getElementsByTagName("BODY")[0]; 
        if(chartId == null)
            chartId = getRandomInt();
        if (className == null)
            className = 'linechart';
        if (width == null)
            width = container.clientWidth;
        // Create HTML elements for new chart. The chartId used in id of div as 'line_chart_x',
        // in id of canvas 'db_canvas_x', legend 'db_legend_x'.
        this.chartDiv = addNewElement(container, 'div', {
            class: className,
            id: 'chart_' + chartId,
            chart_id: chartId,
            style: 'float:left; width:' + width + 'px; padding-bottom:8px;'
        });
        // Create title for chart if parameter title is not null.
        if (title != null) {
            let titleElement = addNewElement(this.chartDiv, 'label', {
                class: 'chart-title nowrap',
                style: 'width:'+ (width - 20)+ 'px; text-align:center; display:block; margin-left:10px; margin-top:10px',
            });
            titleElement.innerHTML = title;
        } 
        // Calculate real width of canvas based on width of chart-div. Think about margin
        // of canvas 30 = 17(margin-left) + 13(margin-right)
        var canvasWidth = width - 30;
        // Set height of canvas based on width
        var canvasHeight = 300;
        if (isMobil) {
            canvasHeight = 220;
        } else {
            if (width < 200) {
                canvasHeight = 200;
            }
            else if (width < 300) {
                canvasHeight = 220;
            }
            else if (width < 400) {
                canvasHeight = 240;
            }
            else if (width < 500) {
                canvasHeight = 260;
            }
            else if (width < 600) {
                canvasHeight = 280;
            }
        }
        // Add canvas and save reference to this one.
        this._canvas = addNewElement(this.chartDiv, 'canvas', {
            id: 'chart_' + chartId+ '_canvas',
            width: canvasWidth,
            height: canvasHeight,
            style: 'margin: 15px 17px 3px 13px;'
        });
        // Set type of axis-X "number" or "time".
        if(axisXType != null || (typeof axisXType) == "undefined" ) {
            if(axisXType == "number") {
                this.axisXType = "number";
            } else if(axisXType == "time") {
                this.axisXType = "time";
            } else {
                logger.warn(`Unknow value axisXType=${axisXType}. Set default "time".`)
                this.axisXType = "time";
            }
        } else {
            this.axisXType == "time";
        }
        // Used to show live data by mousemove over the canvas.
        this._canvas.dataset.axisXType = this.axisXType;
        this._canvas.dataset.chartId = chartId.toString();
        // Save reference to context.
        this._context = this._canvas.getContext('2d');
        // I don't know how it work, but this command disable/reduce antialiasing!
        // It works only if lines are 1 pixel! Need if canvas created or size changed
        // and before draw on canvas.
        this._context.translate(0.5, 0.5);
        // Create element to show data by mousemove and set default '-'.
        if (!isMobil) {
            // Add events processing for canvas and show live data (value and data-time)
            // By leave the canvas remove data.
            this._canvas.addEventListener("mouseout", function (event) {
                // Select element which show data.
                var element = document.getElementById('chart_' + this.dataset.chartId+'_livedata');
                if (element == null)
                    return;
                element.innerText = '-';
            });
            // By mousemove over canvas show value and date-time in special element.
            this._canvas.addEventListener('mousemove', function (event) {
                // Select element which show data.
                var element = document.getElementById('chart_' + this.dataset.chartId+'_livedata');
                if (element == null)
                    return;
                // If current position below axe-X or left of axe-Y no think to do.
                if (this.height - event.offsetY < this.dataset.paddingBottom || event.offsetX < this.dataset.paddingLeft) {
                    element.innerText = '-';
                    return;
                }
                // Calculate position on axe-y to value (return if invalid result) and reduce amount of decimal digits.
                var curValY = this.dataset.maxVal - event.offsetY / parseFloat(this.dataset.coefVal);
                if (isNaN(curValY)) {
                    element.innerText = '-';
                    return;
                }
                if (curValY < 100) {
                    curValY = curValY.toFixed(2);
                }
                else if (curValY < 200) {
                    curValY = curValY.toFixed(1);
                }
                else {
                    curValY = Math.round(curValY);
                }

                // Format of data depend on type of axis-x
                if(this.dataset.axisXType == "time") {
                    // Calculate position on axis-x to data-time. If result invalid return.
                    var curTs = parseInt(this.dataset.minTs) + Math.round((event.offsetX - parseInt(this.dataset.paddingLeft)) / parseFloat(this.dataset.coefTs));
                    if (isNaN(curTs)) {
                        element.innerText = '-';
                        return;
                    }
                    // From Unix-Timestamp create data object that contains separate elements of data-time.
                    curTs = new Date(curTs * 1000).separate();
                    // Set current value and data-time.
                    element.innerHTML = 'Date <b>'+ curTs.mday+ '.'+ curTs.month+ '.'+ curTs.year+ '</b> Time <b>'+ curTs.hour+ ':'+ curTs.min+ 
                        '</b> Value <b>'+ curValY+ '</b>';
                } else {
                    var curValX = parseInt(this.dataset.minTs) + Math.round((event.offsetX - parseInt(this.dataset.paddingLeft)) / parseFloat(this.dataset.coefTs));
                    element.innerHTML = 'Axis-X <b>'+ curValX+ '</b> Axis-Y <b>' + curValY + '</b>';
                }
            });
            addNewElement(this.chartDiv, 'div', {
                class: "chart-live-data, nowrap",
                id: 'chart_' + chartId+ '_livedata',
                style: "margin-left: 12px; margin-right: 12px; margin-bottom: 4px; max-width:95%;",
                innerText: '-'
            });
        }
        // Add div for legend. Each item in the legend is separate sub-div.
        this._legendDiv = addNewElement(this.chartDiv, 'div', {
            class: 'chart-legend',
            id: 'chart_' + chartId+ '_legend'
        });
        // The number used in ID of DOM element for the chart
        this.chartId = chartId;
        // Array of objects that represent lines.
        this.lines = [];
        // Integer. Begin of time that show chart. Sec. Unix Time-stamp. Later calculate
        // from data.
        this._minTs = Number.MAX_SAFE_INTEGER;
        // Integer. End of time that show chart. Sec. Unix Time-stamp. Later calculate
        // from data.
        this._maxTs = 0;
        // Minimal measuring value. Later calculate from data.
        this._minVal = Number.MAX_VALUE;
        // Maximal measuring value. Later calculate from data.
        this._maxVal = Number.MIN_VALUE;
        // Distance between two labels on axis Y. Real value, not a pix! Later calculate 
        // from data.
        this._axeYLabelStep = null;
        // Default and minimal distance (pixel) between labels on axe-y. Later calculate
        // from data.
        this._axeYLabelHeight= 30;
        // Default distance (pixel) between labels on axe-x. Later calculate
        // from data suitable value.
        this._axeXLabelWidth = 80;
        // Coefficient to convert time-stamp to coordinate X. Later calculate from data.
        this._coefTs = null;
        // Coefficient to convert values to coordinate Y. Later calculate from data.
        this._coefVal = null;
        // Padding in canvas where to place labels. Later adjust to real width of labels.
        this._padding = { left: 50, bottom: 35 };
        // Instead automatic, the averadge step on axis X may be set manual. Several measured 
        // values are combined to flatten peaks in the graph. Value in seconds. Optional. 
        // If NaN, not used.
        this.avgStep = NaN;
        
         
    // End consctructor object LineChart.
    }

    /**
     * Add new line and create new item in legend. 
     * @parameter lineData Object. The object represent data for one line in chart.
     * @return Boolean. True if no problem detected. Else false.
     */
    addLine (lineData) {
        if (lineData.values == null) {
            logger.error('Function LineChart.newLine(). DataString is null.');
            return false;
        }
            
        if (!isString(lineData.values)) {
            logger.error('Function LineChart.newLine(). DataString is not a string.');
            return false;
        }
        // Create new line object from part of backend's response. The  measuring data
        // placed in line.real
        var line = this._createLine(lineData);
        if (line == null) {
            logger.error('Function LineChart.newLine(). Function _createLine() return null.');
            return false;
        }
        // Create legend-item as dom-element. 
        line.legend = addNewElement(this._legendDiv, 'div', {
            class: 'chart-legend-item nowrap', 
            id: `chart_${this.chartId}_legend_${lineData.par_id}`,
            style: `cursor: pointer; max-width:${this.chartDiv.clientWidth - 20}px;`,
            innerHTML: `<span class="chart-legend-color" style="background-color:${line.color}; color:${line.color};">--</span>${line.objName} / ${line.parName}`
        });
        // Set function called by click on element to hide/show line on chart. 
        line.legend.addEventListener('click', function (event) {
            
            // First sub-element of class chart-legend-color is colored tag for this legend item. 
            // This element used to swith color by click on legent item.
            var legendColor = event.target.getElementsByClassName('chart-legend-color')[0];

            // By click on colored tag this varibale is null. In this case only exit.
            if(legendColor == null) return;
            
            if(line.hidden == false) {
                line.hidden = true;
                legendColor.style.backgroundColor = 'white';
            } else {
                line.hidden = false;
                legendColor.style.backgroundColor = line.color;
            }
            
            event.target.chart.draw();
        });
        
        // Link to parent chart in the legend item. Need to process click on legend item.
        line.legend.chart = this;

        // Put new line array of lines.
        this.lines.push(line);
        return true;
    }
    
    /**
     * Update data in chart and redraw this one. All items in legend stay the same!
     * @parameter lines Array of objects of type SingleLine.
     * @return true if no problem detected, else false.
     */
    updateLines (lines) {
        if (lines == null) {
            logger.error('Function LineChart.updateLines(). Parameter lines is null.');
            return false;
        }
        if (!Array.isArray(lines)) {
            logger.error('Function LineChart.updateLines(). Parameter lines is not a array.');
            return false;
        }
        // Reset to default some parameters of chart.
        this.lines = [];
        this._minTs = 99999999999999;
        this._maxTs = 0;
        this._minVal = 999999999999999;
        this._maxVal = 0;
        this._axeYLabelStep = null;
        this._coefTs = null;
        this._coefVal = null;
        // Process lines. Each new line add to array of lines.
        for (var i in lines) {
            var line = this._createLine(lines[i]);
            if (line != null) {
                this.lines.push(line);
            }
        }
        this.draw();
        return true;
    }
    
    /**
     * Create line for chart and update some properties of chart minTs, maxTs, minVal, maxVal.
     * @parameter lineData Object.
     * @return SingleLine Object.
     */
    _createLine (lineData) {
        // Create line. Add data later.
        var line = new SingleLine(lineData, this.colors[this.lines.length]);
         
        // Flag. True if value of all items are NaN. In this case set min/max 0/1
        var isAllNaN = true;

        // If par_id is missing, set number of line in the chart as par_id
        if(lineData.par_id == null)
            lineData.par_id = this.lines.length;
        
        // If data-string is empty, return line with flag isEmpty=true.
        if (lineData.values.length == 0)
            return line;
        
        // Split in array and loop over all elements.
        var ar = lineData.values.split(";");

        // Variable used efficiently by loop over big array.
        var arLen = ar.length;
        for (var i = 0; i < arLen; ++i) {
            
            var item = ar[i].split('/');
            
            if (item[0] == "") {
                logger.warn('LineChart._createLine(). Item ' + i + '. Element for axis-x is empty.');
                continue;
            }

            if (isNaN(item[0])) {
                logger.warn('LineChart._createLine(). Item ' + i + '. Element for axis-x is not a number "' + item[0] + '".');
                continue;
            }

            var elem = { ts: null, val: null };
            elem.ts = parseInt(item[0]);
            elem.val = parseFloat(item[1]);
            
            line.real.push(elem);

            // If value is valid number, update min/max and set false isAllNaN.
            if (!isNaN(elem.val)) {
                
                // Update mininal value. Positive and negative values process differently.
                if(elem.val < 0) {

                    if(Math.floor(elem.val * 1.1) < this._minVal) {
                        this._minVal = Math.floor(elem.val * 1.1);
                    }
                    
                } else {
                    
                    if(Math.floor(elem.val * 0.9) < this._minVal)  {
                        this._minVal = Math.floor(elem.val * 0.9);
                    }
                }

                // Update maximal value.
                if (Math.ceil(elem.val * 1.1) > this._maxVal) {
                    this._maxVal = Math.ceil(elem.val * 1.1);
                }

                isAllNaN = false;
            }

            // Update min/max time-stamp
            if (elem.ts < this._minTs)
                this._minTs = elem.ts;
            if (elem.ts > this._maxTs)
                this._maxTs = elem.ts;
        }
        
        // If line is empty skip next operations.
        if (line.real.length == 0)
            return line;
        
        line.isEmpty = false;

        // Adjust min/max values if both 0 or all values NaN
        if ((this._maxVal == 0 && this._minVal == 0) || isAllNaN) {
            this._minVal = 0;
            this._maxVal = 1;
        }

        // Calculate padding. This area used for labels. Both are type of object TextMetrics.
        var minValText = this._context.measureText(this._minVal.toString());
        var maxValText = this._context.measureText(this._maxVal.toString());
        // Set place for label using biggest value.
        if (minValText.width < maxValText.width) {
            this._padding.left = Math.round(maxValText.width) + 23;
        } else {
            this._padding.left = Math.round(minValText.width) + 23;
        }

        return line;
    }    

    /** 
     * Clear all on graphic. Legend stay visible.
     */
    clear () {
        this._context.clearRect(0, 0, this._canvas.width, this._canvas.height);
    }

    /**
     * Draw graphic with all elements.
     */
    draw () {
        // Update coefficient for values and time-stamps only if line is not empty.
        this._coefTs = (this._canvas.width - this._padding.left) / (this._maxTs - this._minTs);
        this._coefVal = (this._canvas.height - this._padding.bottom) / (this._maxVal - this._minVal);
        // Clear canvas
        this._context.clearRect(0, 0, this._canvas.width, this._canvas.height);
        // Return true if all lines in the chart are empty.
        if (this.isEmpty())
            return;
        // Draw axe-x and labels with values
        if(this.axisXType == "time") {
            this._drawAxeXTime();
        } else {
            this._drawAxeXNum();
        }
        // Draw axe-y and time-labels
        this._drawAxeY();
        // Draw each line separate.
        for (var i = 0; i < this.lines.length; i++) {
            this._drawLine(i);
        }
        // Refresh data need to show live-data
        this._canvas.dataset.minTs = this._minTs.toString();
        this._canvas.dataset.coefTs = this._coefTs.toString();
        this._canvas.dataset.maxVal = this._maxVal.toString();
        this._canvas.dataset.coefVal = this._coefVal.toString();
        this._canvas.dataset.paddingLeft = this._padding.left.toString();
        this._canvas.dataset.paddingBottom = this._padding.bottom.toString();
    }    

    /**
     * Change size of chart and redraw. May be used if size of parent panel changed but data 
     * in lines stay the same.
     */
    resize(difWidth, difHeight) {
        this._canvas.width = this._canvas.width + difWidth;
        this._canvas.height = this._canvas.height + difHeight;
        this._context.translate(0.5, 0.5);
        this.draw();
    }

    /**
     * Draw axes-x and labels for time based values.
     */
    _drawAxeXTime () {
        this._drawAxeXBasic();
        // Variable use by loop over differentyl arrays.
        var arLen;
        // Calculate suitable step between labels on axis-x. First get average distance between
        // labels in seconds.
        var stepTs = Math.round(this._axeXLabelWidth / this._coefTs );
        // If step is very small set to minimal step 1 second.
        if(stepTs <= 1) {
            stepTs = 1;
        } else {
            // Select from available steps best suitable.
            arLen = this.stepsX.length - 1;
            for (var i = 0; i < arLen; i++) {
                if (stepTs > this.stepsX[i] && stepTs < this.stepsX[i + 1]) {
                    stepTs = this.stepsX[i];
                    break;
                }
            }
        }
        // Calculate time-stamp of first label
        //  Get time of day of minimal time-stamp
        var tsLabel = this._getBeginOfDayFromUnixTs(this._minTs);
        var isFound = false;
        // Limit time-stamp of minimal label to current time.
        // Time-stamp of first label can't be bigger as as current time.
        var tsNow = new Date().getTime() / 1000;
        while (tsLabel < tsNow) {
            if (tsLabel > this._minTs) {
                isFound = true;
                break;
            }
            tsLabel += stepTs;
        }
        // Create array of labels width coordinate-X and time-stamp for label.
        var labels = [];
        var posX, labelStr;
        while (tsLabel < this._maxTs) {
            posX = this._convertTs2PosX(tsLabel);
            labelStr = this._formatTsForLabel(tsLabel, (this._maxTs - this._minTs));
            labels.push({ X: posX, str: labelStr });
            tsLabel = tsLabel + stepTs;
        }
        // Begin draw vertical label-lines and help-lines.
        this._context.beginPath();
        this._context.strokeStyle = '#e6e6e6';
        // Distance between label-line and help-line.
        var halfStep = Math.round(stepTs * this._coefTs / 2);
        // For all lines position Y is the same
        var posY = this._canvas.height - this._padding.bottom;
        arLen = labels.length;
        for (var i = 0; i < arLen; i++) {
            this._context.moveTo(labels[i].X, 0);
            this._context.lineTo(labels[i].X, posY+ 10);
            // Help-line between two labels
            if (labels[i].X - halfStep > this._padding.left + 2) {
                this._context.moveTo(labels[i].X - halfStep, 0);
                this._context.lineTo(labels[i].X - halfStep, posY);
            }
        }
        // Last help-line after last label if not outside canvas
        if (labels[i - 1] != null) {
            if (labels[i - 1].X + halfStep <= this._canvas.width) {
                this._context.moveTo(labels[i - 1].X + halfStep, 0);
                this._context.lineTo(labels[i - 1].X + halfStep, posY);
            }
        }
        this._context.stroke();
        // Type labels on axe-x
        this._context.font = "12px arial";
        arLen = labels.length;
        for (i = 0; i < arLen; i++) {
            // Print first part of label Hour:Min
            this._context.fillText(labels[i].str.hm, labels[i].X - 17, (posY + 17));
            // The second part of label (Day.Mon) print only by first label at label by begin of day.
            if (i == 0) {
                this._context.fillText(labels[i].str.dm, labels[i].X - 17, (posY + 32));
            }
            else {
                if (labels[i].str.dm != labels[i - 1].str.dm) {
                    this._context.fillText(labels[i].str.dm, labels[i].X - 17, (posY + 32));
                }
            }
        }
        this._context.closePath();
    }
    
    /**
     * Draw axis-x and labels for numeric values.
     */
    _drawAxeXNum () {
        this._drawAxeXBasic();
        // Variable use by loop over differentyl arrays.
        let arLen;
       // Find optimal step between labels. First get average value, then find optimal value. 
        let stepX = Math.round(this._axeXLabelWidth / this._coefTs );
        stepX = this._getNextPreferedNumber(stepX);
        // Calculate value of of first label
        let minXLabelVal = this._getNextPreferedNumber(this._minTs);

        // Create array of labels width coordinate-X and value for label.
        let labelsX = [];
        while (minXLabelVal < this._maxTs) {
            labelsX.push({ X: this._convertTs2PosX(minXLabelVal), str: minXLabelVal });
            minXLabelVal = minXLabelVal + stepX;
        }

        // Begin draw vertical label-lines and help-lines.
        this._context.beginPath();
        this._context.strokeStyle = '#e6e6e6';
        // Distance between label-line and help-line.
        let halfStep = Math.round(stepX * this._coefTs / 2);
        // For all lines position Y is the same
        let posY = this._canvas.height - this._padding.bottom;
        arLen = labelsX.length;
        for (i = 0; i < arLen; i++) {
            this._context.moveTo(labelsX[i].X, 0);
            this._context.lineTo(labelsX[i].X, posY+ 10);
            // Help-line between two labels
            if (labelsX[i].X - halfStep > this._padding.left + 2) {
                this._context.moveTo(labelsX[i].X - halfStep, 0);
                this._context.lineTo(labelsX[i].X - halfStep, posY);
            }
        }
        // Last help-line after last label if not outside canvas
        if (labelsX[i - 1] != null) {
            if (labelsX[i - 1].X + halfStep <= this._canvas.width) {
                this._context.moveTo(labelsX[i - 1].X + halfStep, 0);
                this._context.lineTo(labelsX[i - 1].X + halfStep, posY);
            }
        }
        this._context.stroke();

        // Type labels on axe-x
        this._context.font = "12px arial";
        arLen = labelsX.length;
        for (i = 0; i < arLen; i++) {
            
            this._context.fillText(labelsX[i].str, labelsX[i].X, (posY + 20));   
        }
        this._context.closePath();
        
        // End_ drawAxeNum()
    }

    /**
     * Draw asix-x as line.
     */
    _drawAxeXBasic() {
        this._context.strokeStyle = this.colorDefault;
        this._context.lineWidth = 1 ;
        this._context.beginPath();
        this._context.moveTo(this._padding.left, this._canvas.height - this._padding.bottom);
        this._context.lineTo(this._canvas.width, this._canvas.height - this._padding.bottom);
        this._context.stroke();
    }

    /**
     * Calculate coordinates for labels on axis Y. Distance between labels approximately 40px.
     * Draw axe Y, labels and help-lines (the line between two labels). For labels use place
     * in chart.padding.left. Draw one help-line at label and second line between labels.
     */
    _drawAxeY () {
        // No think to do if coefficients are null. May be the chart contains only one
        // empty line (without measuring-data). In this case don't need draw axes.
        if (this._coefTs == null || this._coefVal == null)
            return;
        // Variable used by efficiently loop over big array.
        var arLen;
        // How much labels with default height fit into current axe Y.
        var labelsAmount = (this._canvas.height - this._padding.bottom) / this._axeYLabelHeight;
        // Find optimal step between labels. First get average value, then find optimal value. 
        this._axeYLabelStep = (this._maxVal - this._minVal) / labelsAmount;
        this._axeYLabelStep = this._getNextPreferedNumber(this._axeYLabelStep);
        // Find value of first label. It is not chart.minVal! It is next
        // suitable value. Consider that chart.MinVal may be negative.
        var labelMinVal = 0;
        if (this._minVal < 0) {
            while (labelMinVal > this._minVal) {
                if (labelMinVal <= this._minVal)
                    break;
                labelMinVal -= this._axeYLabelStep;
            }
        } else if (this._minVal > 0) {
            while (labelMinVal < this._maxVal) {
                if (labelMinVal >= this._minVal)
                    break;
                labelMinVal += this._axeYLabelStep;
            }
        }
        // Calculate maximal hight of axe-y. This value used to prevent labels on axe-y
        // below axe-x. This may happy some times if this._minVal is negative.
        var axeYHight = this._canvas.height - this._padding.bottom;

        // Create array of labels. Each item in array contains {value:value, Y:position-Y}
        // It will be used to draw labels and help-lines. The loop limit to 33 labels to
        // prevent infinite loop.
        var axeY = [];
        i = 0;
        var labelPosY, labelValue;

        while (i < 33) {

            labelValue = labelMinVal + (this._axeYLabelStep * i);

            // If step less as 1 or =2.5, format values of label to two decimal digits
            if (this._axeYLabelStep < 1 || this._axeYLabelStep == 2.5)
                labelValue = labelValue.toFixed(2);
            
            labelPosY = this._convertVal2PosY(labelValue);
            
            // End loop if reach uper border of axe-y.
            if (labelPosY < 0)
                break;
            
            // Some time if this._minVal is negative, label at the bottom placed below axe-x.
            // To prevent it add label to array only if label's position is smaler as available 
            // hight of chart.
            if(labelPosY < axeYHight) {
                var label = { value: labelValue, Y: labelPosY };
                axeY.push(label);
            }

            i++;
        }
        // Begin drawing.
        this._context.lineWidth = 1;
        this._context.beginPath();
        // Draw axe-Y
        this._context.strokeStyle = this.colorDefault;
        this._context.moveTo(this._padding.left, 0);
        this._context.lineTo(this._padding.left, this._canvas.height - this._padding.bottom);
        this._context.stroke();
        // Create horizontal label-lines and help-lines. Begin from bottom.
        this._context.beginPath();
        // Color for label-lines and help-lines. Gray 90%
        this._context.strokeStyle = '#e6e6e6';
        // Position Y for help-lines. (The line between two help-lines)
        var helpLinePosY;
        arLen = axeY.length;
        for (i = 0; i < arLen; i++) {
            // Draw help-line between labels
            helpLinePosY = axeY[i].Y + Math.round(this._coefVal * this._axeYLabelStep / 2);
            if (helpLinePosY < this._canvas.height - this._padding.bottom) {
                this._context.moveTo(this._padding.left, helpLinePosY);
                this._context.lineTo(this._canvas.width, helpLinePosY);
            }
            // Create help-line where label defined. Skip if help-line on axe X.
            if (axeY[i].Y >= this._canvas.height - this._padding.bottom)
                continue;
            this._context.moveTo(this._padding.left, axeY[i].Y);
            this._context.lineTo(this._canvas.width, axeY[i].Y);
        }
        // If possible create help-line after last top label
        helpLinePosY = axeY[axeY.length - 1].Y - Math.round(this._coefVal * this._axeYLabelStep / 2);
        if (helpLinePosY > 0) {
            this._context.moveTo(this._padding.left, helpLinePosY);
            this._context.lineTo(this._canvas.width, helpLinePosY);
        }
        // Draw all created lines
        this._context.stroke();
        // Draw labels on axe Y
        this._context.font = "12px arial";
        this._context.strokeStyle = this.colorDefault;
        // Print labels on left site of axe Y and under the line.
        // The position defined left bottom corner of text-box.
        arLen = axeY.length;
        for (i = 0; i < arLen; i++) {
            this._context.fillText(axeY[i].value, 0, axeY[i].Y + 10);
        }
    }

    /**
     * Draw one line on existing graphic. Used afte add new line.
     * @param lineIndex Number. Index of line in array this.lines[]
     */
    _drawLine (lineIndex) {
        // Create local reference to processed line.
        var line = this.lines[lineIndex];
        if (line == null) {
            logger.error("Function LineChart.drawLine(lineId). The line with ID " + lineIndex + " not found!");
            return;
        }
        // If line is empty or flag hidden is true return.
        if (line.isEmpty == true || line.real.length == 0 || line.hidden)
            return;
        // Array with position of points X/Y used to draw line.
        var pos = [];
        // Used to detect items with the same position X.
        var lastX = -1;
        // Temporary array used to calculate average values.
        var avgY = [];
        // Variable used by efficiently loop over big array.
        var arLen;
        // Array contains result of calculating average values or if empty replace by
        // reference to real data. So by calculating position X/Y used the same array.
        var lineData = [];
        // If average-step is a number, calculate average values.
        if (!isNaN(this.avgStep)) {
            // Variable contains end-time (Unix Time-Stamp) of current average step.
            // At begin set to begin of day of first item. In next step calculate
            // first suitable time-point.
            var avgNextPoint = this._getBeginOfDayFromUnixTs(line.real[0].ts);
            while (avgNextPoint < line.real[0].ts) {
                avgNextPoint += this.avgStep;
            }
            arLen = line.real.length;
            for (var x = 0; x < arLen; x++) {
                if (line.real[x].ts < avgNextPoint) {
                    avgY.push(line.real[x].val);
                } else {
                    // If item don't belong to the same time-range, make time-point
                    // bigger till suitable value. To prevent endless loop limit to
                    // 100 times.
                    var maxCounter = 0;
                    while (line.real[x].ts > (avgNextPoint + this.avgStep)) {
                        avgNextPoint += this.avgStep;
                        maxCounter++;
                        if (maxCounter > 1000) {
                            logger.warn("drawLine(). Can't calculate average values for this line. " + new Date(line.real[x].ts * 1000).formatISOLocal() + '/' + line.real[x].val);
                            return;
                        }
                    }
                    // Create item for this time-range.
                    var item = { ts: avgNextPoint, val: getAvgValue(avgY, 2) };
                    // Add resulting item to array with avg-data.
                    lineData.push(item);
                    // Reset temporary array.
                    avgY = [];
                    // Add next value to temporary array
                    avgY.push(line.real[x].val);
                    // Set next time-point
                    avgNextPoint += this.avgStep;
                }
            }
            // If temporary array not empty, add rest to result.
            if (avgY.length > 0) {
                var item = { ts: avgNextPoint, val: getAvgValue(avgY, 2) };
                lineData.push(item);
            }
        }
        // Clean temporary array
        avgY = [];

        // If array with average values is empty set reference to real-data instead.
        if (lineData.length == 0)
            lineData = line.real;

        // Prepare position x/y for points/items in line.
        arLen = lineData.length;
        for (var i = 0; i < arLen; i++) {

            // Create empty point/item in line.
            var item = { X: NaN, Y: NaN };
            
            // Convert time-stamp to coordinate X
            item.X = this._convertTs2PosX(lineData[i].ts);

            // Convert value to coordinate Y
            if (!isNaN(lineData[i].val)) 
                item.Y = this._convertVal2PosY(lineData[i].val);

            // By items with the same position X calculate average value.
            if (item.X == lastX) {
                avgY.push(item.Y);
                continue;
            } else {
                lastX = item.X;
                if (avgY.length > 0 && i > 0) {
                    avgY.push(pos[pos.length - 1].Y);
                    pos[pos.length - 1].Y = getAvgValue(avgY, 2);
                    avgY = [];
                }
            }
            // Save new item in array used to draw line.
            pos.push(item);
        }
        // Begin draw line
        this._context.strokeStyle = this.colors[lineIndex];
        this._context.lineWidth = 1;
        this._context.beginPath();
        // Set first position to first valid measuring value
        arLen = pos.length;
        for (i = 0; i < arLen; i++) {
            if (!isNaN(pos[i].Y)) {
                this._context.moveTo(pos[i].X, pos[i].Y);
                i++;
                break;
            }
        }
        while (i < arLen) {

            // If valu is valid number containe to draw line.
            if (!isNaN(pos[i].Y)) {
                this._context.lineTo(pos[i].X, pos[i].Y);
                i++;
            } else {
                // Case value is no a number. Different processing depend on flag connectIfMissing
                if (this.connectIfMissing == true) {
                    // Case connect "begin" and "end" of missing range.
                    while (i < pos.length) {
                        if (!isNaN(pos[i].Y)) {
                            break;
                        }
                        i++;
                    }
                } else {
                    // Case don't draw line where data missing
                    this._context.stroke();
                    this._context.beginPath();
                    
                    while (i < pos.length) {
                        if (!isNaN(pos[i].Y)) {
                            break;
                        }
                        i++;
                    }
                }
            }
        }
        this._context.stroke();
    }

    /** 
     * Get next prefered number from 1-2-5 serie. Min 1E-09 max 1E18. Used to calculate optimal
     * step on axis Y and X if type "number". See https://en.wikipedia.org/wiki/Preferred_number
     * Or used to get value of fist label on axis-x. 
     * @param num {number} If 0 retrun 0.
     * @returns number or null if step outside  allowed values.
    */
    _getNextPreferedNumber(num) {

        if(num == 0)
            return 0;

        // E1 preferred number series (1-2-5 series)
        let steps = [1E-9,2E-9,5E-9,1E-8];
        let factor = 1;
        let minStep, maxStep;
        while (factor < 1E18+1) {
            for (var i = 0; i < (steps.length - 1); i++) {
                minStep = steps[i] * factor;
                maxStep = steps[i + 1] * factor;
                if (minStep <= num && num <= maxStep) {
                    //logger.info(`Counter c=${c}, avg step=${step}, new step=${maxStep}`);
                    return maxStep;
                }
            }
            factor = factor * 10;
        }
        return null;
    }
    
    /** 
     * Return true if all lines are empty
     */
    isEmpty () {
        var isEmpty = true;
        for (var i = 0; i < this.lines.length; i++) {
            if (!this.lines[i].isEmpty) {
                isEmpty = false;
            }
        }
        return isEmpty;
    }

    /**
     * Calculate from time-stamp position X using coefficient for axe X and available width.
     */
    _convertTs2PosX (ts) {
        return (this._canvas.width - Math.round((this._maxTs - ts) * this._coefTs));
    }

    /**
     * Calculate from measuring value position Y using coefficient for axe Y and available height.
     * Return rounded integer.
     */
    _convertVal2PosY (val) {
        return (Math.round((this._maxVal - val) * this._coefVal));
    }
    
    /**
     * Convert unix time-stamp to object that contains two elements. This object used as label on 
     * Axe-X. Result is dependet on difference. By small difference in first line show hour:min,
     * second line day.mon. By difference between 3-180 days in first line show day.mon in second
     * line yaer. By difference > 180 days in first line show month in second line yaer. Day and 
     * month in format DD.MM always two digits. Hours and minutes in format HH:MM always two digits.
     * @param ts Number. Unix-time-stamp.
     * @param tsdif Number. Difference between first and last timestamp
     * @return Object. The object used to create labels on axe-x.
     */
    _formatTsForLabel (ts, tsdif) {
        let date = new Date(ts * 1000);
        
        // Recalculate difference from seconds to days.
        tsdif = tsdif / (3600 * 24);

        let yaer = date.getFullYear();

        let mon = date.getMonth() + 1;
        if (mon < 10)
            mon = '0' + mon.toString();

        let mday = date.getDate();
        if (parseInt(mday) < 10)
            mday = '0' + mday.toString();

        let hours = date.getHours();
        if (parseInt(hours) < 10)
            hours = '0' + hours;

        let min = date.getMinutes();
        if (parseInt(min) < 10)
            min = '0' + min.toString();

        let result = {};
        if(tsdif < 3) {
            result = { dm: mday + '.' + mon, hm: hours + ':' + min };

        } else if(tsdif >= 3 && tsdif < 180) {
            result = { dm: yaer, hm: mday+ '.'+ mon };

        } else {
            result = { dm: yaer, hm: ' '+ mon};
        }

        return result;
    }
    
    /**
     * Calculate begin of day in seconds from unix time-stamp in seconds.
     * @param ts {number} unix time-stamp in seconds
     * @return {Number}
     */
    _getBeginOfDayFromUnixTs (ts) {
        var result = new Date(ts * 1000);
        result = new Date(result.getFullYear(), result.getMonth(), result.getDate());
        result.setMinutes(59);
        result.setSeconds(59);
        result.setMilliseconds(1000);
        return result.getTime() / 1000;
    }

    // Set of colors used to draw lines
    colors = [
        '#0044CC', // Blue 65%,
        '#008000', // Green
        '#FF8533', // Orange
        '#6A6A6A', // Black 25%
        '#C71585', // MediumVioletRed
        '#8B4513', // SaddleBrowx
        '#1E90FF', // DodgerBlue
        '#FF4500', // OrangeRed
        '#00FF00', // Lime
        '#8A2BE2'  // BlueViolet
    ]
        
    // Default color used in some elements
    colorDefault = '#657585';
    
    // Array of possible steps on axis-X in seconds
    stepsX = [1, 2, 5, 10, 20, 30, 1*60, 2*60, 5*60, 10*60, 15*60, 20*60, 30*60, 
        60*60, 2*60*60, 4*60*60, 6*60*60, 12*60*60, 24*60*60, 2*24*60*60, 3*24*60*60, 
            4*24*60*60, 5*24*60*60, 7*24*60*60, 10*24*60*60, 15*24*60*60, 20*24*60*60, 
                30*24*60*60, 45*24*60*60, 60*24*60*60, 90*24*60*60, 120*24*60*60];
    
    // End class LineChart
}

/**
 * The object represent a line in chart. 
 * @class
 * @param lineData {LineData}
 * @param color {string} color in HTML format, name or hex.
 */
class SingleLine {
    constructor(lineData, color) {
        this.parId = lineData.par_id;     
        this.parName = lineData.par_name != null ?  lineData.par_name : ""; 
        this.objName = lineData.obj_name != null ? lineData.obj_name : ""; 
        this.real = []; // Empty array for pairs ts/val. "ts" is unix time-stamp, "val" is measuring value.
                    
        // Set connectIfMissing. Default false.
        if(lineData.connectIfmissing == null) {
			this.connectIfmissing = false;
        } else {
        this.connectIfmissing = lineData.connectIfmissing; // By false don't draw line if data missing
        }
        
        this.isEmpty = true; // Flag indicate that line contains no data.
        this.hidden = false; // Property uses to hide line in chart by click on legend item.
        
        if(color == null) {
            this.color = "black"; // Default color for line
        } else {
            this.color = color;
        }

        this.legend = null; // HTML-Element to show one elemen in chart's legend. Init later.
    }
}

/**
 * The object represent data to create one line in chart.
 * @class
 * @param obj_name {string} is visible in legen of line.
 * @param par_name {string}, tring is visible in legen of line.
 * @param par_id {number} internal id of line. Optional. May be used to request data from backend.
 * @param values {string}, elements separates by ";". Each element containing time-stamp and 
 * meassuring value separated by "/". eg. "161111145730/3.48;161111145843/3.50;161111150013/"   
 * Value may be in range Number.MIN_VALUE till Number.MAX_VALUE or may be missing. The last element 
 * must be without value. It indicating where is end of time-period for this line.
 * @param connectIfMissing {boolean} if false don't draw line where data are missing, else connect
 * begin and end of range where data are mising. Default false.
 * @example
 * line = {
 *     obj_name:"Proxy Server",
 *     par_name:"CpuTotal",
 *     par_id:"2452",
 *     values: "161111145730/3.48;161111145843/3.50;161111150013/",
 *     connectIfMissing: true
 * }
 */
class LineData {
    constructor(obj_name, par_name, par_id, values, connectIfMissing) {
       this.obj_name = obj_name;
       this.par_name = par_name;
       this.par_id = par_id;
       this.values = values;
       this.connectIfMissing = connectIfMissing ;
    }
}
