# adaptive-linechart :chart_with_upwards_trend:
Version 1.3.0

This is a small library to draw charts in the browser. All elements of chart will be set up 
automaticaly based on data. You can adjust the layout of the chart by using external CSS styles
for .nowrap .linechart, .chart-title, .chart-live-data, .chart-legend, .chart-legend-item, .chart-legend-color. Maximal 10 lines in the chart. The values on axis-x may be Unix time-stamp or numercal. The chart uses the global variable isMobil (Boolean). If false, live data are shown by "mouse-over". By click on item in the legend hide/show line on chart. The library has own simple logger that writes messages to the console. If needed, you can rewrite this logger and redirect messages to another target.

Additional supplied function to convert CSV to time-series.


# Example

This example creates a chart with 3 lines in parent element with ID 'charts':
```js
var chartData = [
    {
        obj_name: 'Stock preis',
        par_name: ' Open',
        values: '1565301600/138.6100;1565560800/137.0700;1565647200/136.0500;1565733600/136.3600;1565820000/134.3900'
    },
    {
        obj_name: 'Stock preis',
        par_name: ' Hight',
        vaues: '1565301600/139.3800;1565560800/137.8600;1565647200/138.8000;1565733600/136.9200;1565820000/134.5800'
    },
    {
        obj_name: 'Stock preis',
        par_name: ' Low',
        values:  '1565301600/136.4600;1565560800/135.2400;1565647200/135.0000;1565733600/133.6700;1565820000/132.2500'
    }
];

var lchart =  new LineChart(document.getElementById('charts'),null, null, 'Example Chart-Line', null);

for(i=0; i < chartData.length; i++) {
    lchart.addLine(chartData[i]);
}
            
lchart.draw();
```

[Live example](https://www.softdorado.com/adaptive-linechart.html)

# Public methods

**`LineChart(container, chartId, width, title, className, axisXType)`** - create an object of type LineChart 
and HTML code for the chart.
- **container** - Reference to parent element for chart. If null: use BODY as parent element.
- **chartId** - String. Identifier for the chart. Optional. If not defined use random integer.
- **width** - Integer. Width in pixel. Optional. Default use available width in parent element. Height set automaticaly.
- **title** - String. Title of chart. Optional. If null: don't create title.
- **className** - String. Class for div of chart. Optional. Default "linechart".
- **axisXType** String. For time based values on axis X set "time" (Default). For number based values "numer". 

**`addLine(lineData)`** - add data for a new line and automatically create a new item in the legend. 
Return true if no problem detected, otherwise false.

**`lineData`** - The object represents data for one line in the chart.
- **obj_name** - String. Name of object related to the chart. Used in legend.
- **par_name** - String. Name of the parameter that represents the current line. Used in legend.
- **par_id** - Integer. Optional ID of this line. May be used to request data from backend.
- **values** - String.  If axisXType "time" format "Unix-Timestamp/Value;Unix-Timestamp/Value;..".
                        If axisXType "number" format "Number X/Number Y; Number X/Number Y/..." 
                        For number the step min. 1E-09 max. 1E18.
- **connectIfMissing** - Boolean. If false don't draw line where data are missing, else connect 
  begin and end of range where data are mising. Default false.

**`draw()`** - draw graphic. Skip lines if properties hidden=true or isEmpty=true.

**`clear()`** - clear graphic. Legend stays visible.

**`resize(difWidth, difHeight)`** - Change size of chart and redraw. May be used if size of parent 
panel changed but data in lines stay the same.
- **difWidth** - Signed integer. Difference in width.
- **difHeight** - Signed integer. Difference in height.

**`updateLines(lines)`** - Update data in chart and redraw this one. Return true if no problem 
detected, else false. May be used if data inlines changed.
- **lines** - Array of lineData.

**`isEmpty()`** - Return true if all lines are empty

**`avgStep`** - Set time interval of measurement data on graph. Several measured values are 
combined to flatten peaks in the graph. value in seconds. Optional.

**`lines[]`** - array containing all lines

**`chartId`** - chart Id. Used to reference exactly to object or HTML element.



## Utility

**`scvToTimeSeries(csv)`** - process CSV and return array of objects 

`{name: "Name of serie", data: "Unix-Timestamp/Value;Unix-Timestamp/Value"}`. 

Assume the first column contains data-time as string. The CSV may contain many columns. Name of 
serie taken from header. Values may be empty. This function needs the package [Moment.js](https://momentjs.com)

## Contact

If you have any questions, please contact me `adaptive-linechart@my.mail.de`

## License

[MIT](https://opensource.org/licenses/MIT) © Andrej Koslov

