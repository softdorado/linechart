# Prepare files for distribution

* Update version in README, package.json, linechart.js
* Put information about changes in CHANGELOG

```
npm -i terser -g

terser src\linechart.js src\chart-util.js -o dist\linechart-all.min.js
type src\linechart.css > dist\linechart.css
```
# Uplod to NPM
```
npm publish
```


