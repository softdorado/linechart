document.addEventListener('DOMContentLoaded', function () {
    createCharts();
});

var isMobil = false;
var charts = [];

function createCharts () {
    
    // Parent element for charts
    let parent = document.getElementById('charts');

    // Create chart with numerical axis-x and attach to array
    let lchart = new LineChart(parent,null , null, 
        'Example with numerical values on axis-x', null, "number");
    charts.push(lchart);

    let lineData1 = {
        obj_name: "Line one",
        values: ""
    };

    // Create two examples as math. function. Last ";" don't need
    for(i = 0; i < 91 ; i++){
        lineData1.values += i+ "/"+ (i* Math.cos(i))+";";
    }
    lineData1.values = lineData1.values.substring(0, lineData1.values.length -1); 
    lchart.addLine(lineData1);

    let lineData2 = {
        obj_name: "Line two",
        values: ""
    };
    for(i = 0; i < 90 ; i++){
        lineData2.values += i+ "/"+ (i* Math.cos(i) * 2)+";";
    }
    lineData2.values = lineData2.values.substring(0, lineData2.values.length -1); 
    lchart.addLine(lineData2);

    lchart.draw();
    
    // Extract data from CSV and create new chart with time-based axis-x.
    let request = new XMLHttpRequest();
    request.open("GET","example.csv");
    request.addEventListener('load', function(event) {
        if (request.status == 200) {
           
            let arScvData = scvToTimeSeries(request.responseText);
            lchart =  new LineChart(document.getElementById('charts'),null, null, 
                "Example data from CSV and data-time on axis-x.", null, "time");
            charts.push(lchart);

            for(let i=0; i < arScvData.length - 1; i++) {
                let line = {
                    obj_name: 'Stock preis',
                    par_name: arScvData[i].name,
                    values: arScvData[i].data
                };
                lchart.addLine(line);
            }
            
            lchart.draw();
        } else {
            parent.innerHTML("Failed get CSV. "+ request.statusText+ " Responce "+ request.responseText);
        }
    });

    request.send(); 
}
                    